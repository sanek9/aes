#include <stdio.h>
#include <stdlib.h>
#include <math.h>
unsigned char Sbox[256];
unsigned char InvSbox[256];
unsigned char Rcon[11]={0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36};
unsigned char Mix[]={0x02,0x03,0x01,0x01};
unsigned char InvMix[]={0x0e,0x0b,0x0d,0x09};
unsigned char getProd(unsigned char b, unsigned char d);
unsigned char getSb(unsigned char b);
int encrypt(unsigned char input[], int s_i, unsigned char **output, unsigned char *key);
#define Nb 4
#define Nk 4
#define Nr 10
//int Nb 	=4;
//int Nk	=4;
//int Nr	=10;

int main(int args, char *argv[]){
/*	int kk, ii;
	getSb(1);
	for(ii=0;ii<16;ii++){
		for(kk=0;kk<16;kk++){
			(getSb((ii<<4)|kk)<16)?printf("0"):0;
			printf("%x ",getSb((ii<<4)|kk));
		}
		printf("\n");
	}
	return 0;
*/	//int Nb=4;
	//int Nk=4;
	//int Nr=10;
	unsigned char key[4*Nk]={0};
	unsigned char input[512]={0};
	unsigned char *output;
	unsigned char cp,cn;
	int m, i,l,j;
	printf("0 - encrypt\n1 - decrypt\n");
	scanf("%d", &m);
	while(0){
		scanf("%c",&cn);
		printf("%d\n",cn);
	}
	switch(m){
		case 0:
			printf("intput string:\n");
			scanf("%s",input);
			printf("input key:\n");
			scanf("%s",key);
			l=strlen(input);
        		l=encrypt(input, l, &output, key);
        		for(i=0;i<l;i++){
				(output[i]<16)?printf("0"):0;
                		printf("%x",output[i]);
			}
			printf("\n");

		break;
		case 1:
			printf("intput string:\n");
			l=0;
                        scanf("%s",input);
                        printf("input key:\n");
                        scanf("%s",key);
			l=strlen(input);
			j=0;
			for(i=0;i<l;i++){
				//cp=input[i-1];
				cn=input[i];
				cn=(cn>=48)? (cn<=57)?cn-48: (cn>=65)? (cn<=70)?cn-65+10: (cn>=97)? (cn<=102)?cn-97+10 :0:0:0:0;
				(i%2)? (input[j]|=cn)|j++: (input[j]=cn<<4)|0;
				//(i%2)? printf("%d ",(cp<<4)|cn):0;
				//cp=cn;
			}
                        l=decrypt(input, j, &output, key);
                        for(i=0;i<l;i++){
				//(input[i]<10)?printf("0"):0;
                                printf("%c",output[i]);
                        }
                        printf("\n");
		break;
		default:
			printf("0 or 1");;
		break;
	}
	return 0;
	l=strlen(input);
	for(i=0;i<l;i++){
                printf("%c",input[i]);
        }
        printf("\n");
	//printf("%d\n",encrypt(input, 16, output, key));
	l=encrypt(input, l, &output, key);
	for(i=0;i<l;i++){
		printf("%x",output[i]);
	}
	printf("\n%d\n",l);
	l=decrypt(output, l, &output, key);
	for(i=0;i<l;i++){
                printf("%c",output[i]);
        }
	printf("\n%d\n",l);
	return 0;

}
int encrypt(unsigned char input[], int s_i, unsigned char **o, unsigned char *key){
	int i, k;
	int s_o;
	int len;
	float g=((s_i+1)/(4.0*Nb));
	fillSbox();
	k=g;
	len =(s_i+1>(4*Nb))? 4*Nb*( (((int)((g-k)*10))>0)+k ) :s_i+1;
	unsigned char *output;
	output=(unsigned char *)calloc(len,sizeof(unsigned char));
	*o=output;
	unsigned char State[4][Nb];
	unsigned char KeySchedule[4][Nb*(Nr+1)];
	KeyExpansion(KeySchedule,key);
	s_o=0;
	State[0][0]=len-s_i;
	while(s_o<len){
		for(k=0;k<Nb;k++){
			for(i=(s_o==0&&k==0)?1:0;i<4;i++){
                        	State[i][k]=input[i+4*k+s_o-1];
			}
		}
		AddRoundKey(State,KeySchedule,0);
		for(i=1;i<Nr;i++){
			SubBytes(State,0);
			ShiftRows(State,0);
			MixColumns(State,0);
			AddRoundKey(State,KeySchedule,i);
		}
		SubBytes(State,0);
		ShiftRows(State,0);
		AddRoundKey(State,KeySchedule,Nr);
		for(i=0;i<4;i++){
                        for(k=0;k<Nb;k++)
                                output[i+4*k+s_o]=State[i][k];
                }
		s_o+=4*Nb;
	}
	return s_o;

}
int decrypt(unsigned char input[], int s_i, unsigned char **o, unsigned char *key){
        int i, k;
        int s_o;
	int len =s_i;
	fillSbox();
	unsigned char *output; 
        output=(unsigned char*)calloc(len,sizeof(unsigned char));
	*o=output;
        unsigned char State[4][Nb];
        unsigned char KeySchedule[4][Nb*(Nr+1)];
        KeyExpansion(KeySchedule,key);
	s_o=0;
        while(s_o<len){
                for(i=0;i<4;i++){
                        for(k=0;k<Nb;k++)
                                State[i][k]=input[i+4*k+s_o];
                }
                AddRoundKey(State,KeySchedule,Nr);
                for(i=Nr-1;i>0;i--){
                        ShiftRows(State,1);
			SubBytes(State,1);
                        AddRoundKey(State,KeySchedule,i);
			MixColumns(State,1);
                }
                ShiftRows(State,1);
		SubBytes(State,1);
                AddRoundKey(State,KeySchedule,0);
		for(k=0;k<Nb;k++){
	                for(i=(s_o==0&&k==0)?(0&(len=s_i-State[0][0])^1):0;i<4;i++){
                                output[i+4*k+s_o-1]=State[i][k];
			}
		}
		s_o+=4*Nb;
	}
        return len;

}

int SubBytes(unsigned char State[][Nb],int inv){
	unsigned char *p=(inv)?(unsigned char *)&InvSbox:(unsigned char *)&Sbox;

	int i, k;
        for (i=0;i<4;i++){
                for(k=0;k<Nb;k++){
                        State[i][k]=p[State[i][k]];
                }
        }

}

int rightShift(unsigned char a[], int n, int m){
	unsigned char swap[2];
	unsigned char s[m];
	int c=0, i, k;
	for(i=0;i<m;i++)
		s[i]=a[i];
	for(k=0;k<m;k++){
		swap[!c]=s[k];
		for (i=m+k;i<n+m;i+=m){
			swap[c]=a[i%n];
			a[i%n]=swap[!c];
			c=!c;
		}
	}

}
int ShiftRows(unsigned char State[][Nb],int inv){
	int i, k;
	char s;
        for (i=1;i<4;i++){
		rightShift(State[i],Nb,(inv>0)?Nb-i:i);
        }

}


int MixColumns(unsigned char State[][Nb],int inv){
	int i, k, j;
	unsigned char s;
	unsigned char *p=(inv)?(unsigned char *)&InvMix:(unsigned char *)&Mix;
        for (i=0;i<Nb;i++){
                for(k=0;k<4;k++){
			s=0;
			for(j=0;j<4;j++){
                        	s^=getProd(State[k][i],p[(4-j+k)%4]);
			}
			State[k][i]=s;
		}
        }

}


int AddRoundKey(unsigned char State[][Nb],unsigned char KeySchedule[][Nb], int round){
	int i, k;
	for (i=0;i<4;i++){
		for(k=0;k<Nb;k++){
			State[i][k]^=KeySchedule[i][round*4+k];
		}
	}
}
int KeyExpansion(unsigned char KeySchedule[][Nb], char *SecretKey){
	int r, c;
	for(c=0;c<4;c++){
		for(r=0;r<Nk;r++){
			KeySchedule[c][r]=SecretKey[r+4*c];
		}
	}
	for(r=Nk;r<(Nb*(Nr+1));r++){
		if(r%Nk==0){
			for(c=0;c<4;c++){
				KeySchedule[c][r]=KeySchedule[c][r-Nk] ^ Sbox[KeySchedule[(c+1)%4][r-1]] ^ ((!c)? Rcon[r/Nk]:0);
			}
		}else{
			for(c=0;c<4;c++){
				KeySchedule[c][r]=KeySchedule[c][r-Nk] ^ KeySchedule[c][r-1];
			}
		}
	}

}

int fillSbox(){
	int i,j;
	for(i=0;i<256;i++){
		Sbox[i]=getSb(i);
		InvSbox[Sbox[i]]=i;
	}

}
unsigned char getProd(unsigned char b, unsigned char d){
	unsigned char s=b;
	unsigned char sb=0;
	while(d){
		if(d&0x1){
			d^=0x1;
			sb^=s;
		}else{
			d=d>>1;
			s=((s<0x80)?s<<1:(s<<1)^0x1b)%0x100;
		}

	}
	return sb;

}

unsigned char getSb(unsigned char b){
	unsigned char d=0xf8, ds=0, bs=0;
	int i,k;
	for(i=0;i<256;i++){
		if(getProd(b,i)==0x1){
			b=i;
			break;
		}
	}
	for (i=7;i>=0;i--){
		bs=0;
		for(k=0;k<8;k++){
			bs^=((d>>k)&0x1) & ((b>>k)&0x1);
		}
		ds|=bs<<i;
		d=d>>1|((d&0x1)<<7);
	}
	return ds^0x63;

}






